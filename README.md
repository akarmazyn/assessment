Try Catch PHP assignment
========================

Hi, welcome to our ‘code’ restaurant! Today we will begin with a small starter that should wake your brain enough to be ready for the main course. This will demand from you a bit more work, more focus and more imagination, but when you finish you will feel good, trust me.
We will finish with a delicious dessert.

Have fun and bon appétit :)