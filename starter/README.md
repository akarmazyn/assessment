Starter - Algorithmic pleasure
==============================

A:
--

​The sum of all natural numbers below 10​ that are multiples of 3 or 5​ are 23​ (3 + 5 + 6 + 9)

Write a php script that will find the sum of all the multiples of 3 or 5​ below 1000​. The script should run from command line and put the result on screen. We will judge this task based on simplicity, efficiency and cleverness of the code.

```
php assignment-a.php
```

Extra: Create a second algorithm to find the sum of all the multiples of 3 and​ 4 below 1000.

```
php assignment-a2.php
```

B:​
--

Make a function that will calculate the power of a number x to index y, but you cannot use multiplication! to make it a bit simpler please take into account only natural numbers.

```
assignment-b.php
```

C:
--

​Calculate and print 10 numbers for fibonacci series. Use recursion.

```
php assignment-c.php
```

Extra: Create second algorithm for fibonacci but without recursion.

```
php assignment-c2.php
```
