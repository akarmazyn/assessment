<?php
function isThreeMultiplier($number) {
    return 0 === $number % 3;
}

function isFiveMultiplier($number) {
    return 0 === $number % 5;
}

function getThreeAndFiveMultiples($limit) {
    $sum = 0;
    for ($i = 1; $i < $limit; $i++) {
        if (isThreeMultiplier($i) || isFiveMultiplier($i)) {
            $sum += $i;
        }
    }

    return $sum;
}

echo getThreeAndFiveMultiples(1000);
