<?php

/**
 * ​Calculate and return $n numbers for fibonacci series, does not use recursion
 *
 * @param int $n
 * @return array
 */
function fibonacci($n) {
    if ($n <= 2) {
        return 1;
    }

    // Need an array indexed from 1 with two elements with 1 value
    $fibonacciNumbers = array_fill(1, 2, 1);
    for ($i = 3; $i <= $n; $i++) {
        $fibonacciNumbers[] = $fibonacciNumbers[$i - 1] + $fibonacciNumbers[$i - 2];
    }

    return $fibonacciNumbers;
}

echo implode(PHP_EOL, fibonacci(10));
