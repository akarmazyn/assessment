<?php

/**
 * ​Calculate and return $n'th number for fibonacci series, uses recursion
 *
 * @param $n
 * @return int
 */
function fibonacci($n) {
    if ($n <= 2) {
        return 1;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

for ($i = 1; $i <= 10; $i++) {
    echo fibonacci($i) . PHP_EOL;
}
