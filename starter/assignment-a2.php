<?php
function isThreeMultiplier($number) {
    return 0 === $number % 3;
}

function isFourMultiplier($number) {
    return 0 === $number % 4;
}

function getThreeAndFourMultiples($limit) {
    $sum = 0;
    for ($i = 1; $i < $limit; $i++) {
        if (isThreeMultiplier($i) || isFourMultiplier($i)) {
            $sum += $i;
        }
    }

    return $sum;
}

echo getThreeAndFourMultiples(1000);
