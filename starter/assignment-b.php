<?php

/**
 * Calculates power of number $x to index $y, does not use multiplication
 *
 * @param $x
 * @param $y
 * @return int
 */
function power($x, $y) {
    if (1 === $x || 1 === $y) {
        return $x;
    }

    $multiplication = function($x, $y) {
        $result = $x;
        for ($i = 1; $i < $y; $i ++) {
            $result += $x;
        }

        return $result;
    };

    $power = 0;
    $base = $x;
    for ($i = 1; $i < $y; $i++) {
        $power = $multiplication($base, $x);
        $base = $power;
    }

    return $power;
}
