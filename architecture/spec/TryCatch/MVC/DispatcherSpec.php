<?php

namespace spec\TryCatch\MVC;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TryCatch\Application;
use TryCatch\Http\Request;
use TryCatch\Router\Route\Literal;
use TryCatch\Router\RouteCollection;

class DispatcherSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\MVC\Dispatcher');
    }

    public function it_throws_exception_on_dispatch(
        Request $request,
        RouteCollection $routes,
        Application $application
    ) {
        $routes->getRoutes()->willReturn([]);

        $this->shouldThrow('\TryCatch\Router\RouterException')->duringDispatch($request, $routes, $application);
    }

    public function it_dispatches_request(
        Request $request,
        RouteCollection $routes,
        Literal $route,
        Application $application
    ) {
        $result = true;
        $callback = function() use ($result) {
            return $result;
        };
        $route->match(Argument::type('string'), Argument::type('null'))->willReturn(true);
        $route->getOptions()->willReturn([$callback, []]);
        $routes->getRoutes()->willReturn([$route]);

        $this->dispatch($request, $routes, $application)->shouldBe($result);
    }
}
