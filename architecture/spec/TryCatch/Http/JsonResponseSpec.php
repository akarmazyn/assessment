<?php

namespace spec\TryCatch\Http;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class JsonResponseSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Http\JsonResponse');
    }

    public function let()
    {
        $this->beConstructedWith($this->getConstructorValues());
    }

    private function getConstructorValues()
    {
        return [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ];
    }

    public function it_stores_proper_values()
    {
        $this->get('key1')->shouldBeEqualTo('value1');
        $this->get('key2')->shouldBeEqualTo('value2');
        $this->get('key3')->shouldBeEqualTo('value3');
    }

    public function it_sets_proper_values_with_set()
    {
        $this->set('key4', 'value4');
        $this->get('key4')->shouldBeEqualTo('value4');
    }

    public function it_replaces_all_values_with_set_values()
    {
        $newValues = [
            'key5' => 'value5',
            'key6' => 'value6'
        ];

        $this->setValues($newValues);

        $this->get('key5')->shouldBeEqualTo('value5');
        $this->get('key6')->shouldBeEqualTo('value6');
    }

    public function it_encodes_proper_json()
    {
        $this->__toString()->shouldBeEqualTo(json_encode($this->getConstructorValues()));
    }
}
