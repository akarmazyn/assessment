<?php

namespace spec\TryCatch\Http;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RequestSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Http\Request');
    }

    public function it_implements_request_interface()
    {
        $this->shouldHaveType('TryCatch\Http\RequestInterface');
    }
}
