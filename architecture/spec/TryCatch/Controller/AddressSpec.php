<?php

namespace spec\TryCatch\Controller;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TryCatch\Application;
use TryCatch\DI\Container;
use TryCatch\Http\Request;
use TryCatch\Model\Address as AddressModel;
use TryCatch\Repository\AddressCsvRepository;
use TryCatch\Validator\Address;

class AddressSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Controller\Address');
    }

    public function it_returns_json_response_on_index(
        Application $application,
        Container $container,
        AddressCsvRepository $repository
    ) {
        $this->setApplication($application);
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);

        $this->index()->shouldHaveType('TryCatch\Http\JsonResponse');
    }

    public function it_can_return_404_response_on_show(
        Application $application,
        Container $container,
        AddressCsvRepository $repository
    ) {
        $this->setApplication($application);
        $repository->getById(Argument::type('integer'))->willThrow(new \TryCatch\Repository\Exception('', 404));
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);

        $result = $this->show([0]);
        $result->shouldHaveType('TryCatch\Http\JsonResponse');
        $result->getStatusCode()->shouldBe(404);
    }

    public function it_returns_data_from_address_model_on_show(
        Request $request,
        Application $application,
        Container $container,
        AddressCsvRepository $repository,
        AddressModel $dataModel
    ) {
        $dataId = 0;
        $dataModel->offsetGet('name')->willReturn('Test name');
        $dataModel->offsetGet('phone')->willReturn('123');
        $dataModel->offsetGet('street')->willReturn('Test street');

        $repository->getById($dataId)->willReturn($dataModel);
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);
        $request->get(Argument::type('string'), Argument::type('bool'))->willReturn($dataId);
        $this->setRequest($request);
        $this->setApplication($application);

        $data = $this->show([$dataId]);

        $data->shouldHaveType('TryCatch\Http\JsonResponse');

        $data->get('name')->shouldBeEqualTo('Test name');
        $data->get('phone')->shouldBeEqualTo('123');
        $data->get('street')->shouldBeEqualTo('Test street');
    }

    public function it_returns_error_on_invalid_create(
        Request $request,
        Application $application,
        Container $container,
        Address $validator
    ) {
        $request->getBody()->willReturn([]);
        $this->setRequest($request);
        $this->setApplication($application);
        $validator->isValid(Argument::type('\TryCatch\Model\Address'))->willThrow('\TryCatch\Validator\Exception');
        $container->offsetGet('address.validator')->willReturn($validator);
        $application->offsetGet('di')->willReturn($container);

        $result = $this->create();

        $result->shouldHaveType('TryCatch\Http\JsonResponse');
        $result->get('error')->shouldBeEqualTo('');
        $result->getStatusCode()->shouldBe(400);
    }

    public function it_returns_ok_on_valid_create(
        Request $request,
        Application $application,
        Container $container,
        Address $validator,
        AddressCsvRepository $repository
    ) {
        $request->getBody()->willReturn([]);
        $this->setRequest($request);
        $this->setApplication($application);
        $validator->isValid(Argument::type('\TryCatch\Model\Address'))->willReturn(true);
        $container->offsetGet('address.validator')->willReturn($validator);
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);

        $result = $this->create();

        $result->shouldHaveType('TryCatch\Http\JsonResponse');
        $result->getStatusCode()->shouldBe(201);
    }

    public function it_returns_404_on_invalid_update(
        Application $application,
        Container $container,
        AddressCsvRepository $repository
    ) {
        $this->setApplication($application);
        $repository->getById(Argument::type('integer'))->willThrow(
            new \TryCatch\Repository\Exception('Address does not exist', 404)
        );
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);

        $result = $this->update([0]);
        $result->shouldHaveType('TryCatch\Http\JsonResponse');
        $result->getStatusCode()->shouldBe(404);
        $result->get('error')->shouldBe('Address does not exist');
    }

    public function it_returns_404_on_invalid_delete(
        Application $application,
        Container $container,
        AddressCsvRepository $repository
    ) {
        $this->setApplication($application);
        $repository->getById(Argument::type('integer'))->willThrow(
            new \TryCatch\Repository\Exception('Address does not exist', 404)
        );
        $container->offsetGet('address.repository')->willReturn($repository);
        $application->offsetGet('di')->willReturn($container);

        $result = $this->delete([0]);
        $result->shouldHaveType('TryCatch\Http\JsonResponse');
        $result->getStatusCode()->shouldBe(404);
        $result->get('error')->shouldBe('Address does not exist');
    }
}
