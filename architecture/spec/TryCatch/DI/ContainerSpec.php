<?php

namespace spec\TryCatch\DI;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ContainerSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\DI\Container');
    }

    public function let()
    {
        $this->beConstructedWith($this->getConstructorValues());
    }

    private function getConstructorValues()
    {
        return [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ];
    }

    public function it_stores_proper_values()
    {
        $this->offsetGet('key1')->shouldBeEqualTo('value1');
        $this->offsetGet('key2')->shouldBeEqualTo('value2');
        $this->offsetGet('key3')->shouldBeEqualTo('value3');
    }
}
