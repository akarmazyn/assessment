<?php

namespace spec\TryCatch\Router\Route;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RegexSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Router\Route\Regex');
    }

    public function let()
    {
        $this->beConstructedWith('#/address/(\d+)#', [], 'get');
    }

    public function it_returns_false_on_bad_match()
    {
        $this->match('get', 'bad_route')->shouldBe(false);
    }

    public function it_returns_true_match()
    {
        $this->match('get', '/address/123')->shouldBe(true);
    }
}
