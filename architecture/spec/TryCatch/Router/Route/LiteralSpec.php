<?php

namespace spec\TryCatch\Router\Route;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LiteralSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Router\Route\Literal');
    }

    public function let()
    {
        $this->beConstructedWith('literal_route', [], 'get');
    }

    public function it_returns_false_on_bad_match()
    {
        $this->match('get', 'bad_route')->shouldBe(false);
    }

    public function it_returns_true_match()
    {
        $this->match('get', 'literal_route')->shouldBe(true);
    }
}
