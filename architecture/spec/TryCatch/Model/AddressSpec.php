<?php

namespace spec\TryCatch\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AddressSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Model\Address');
    }

    public function let()
    {
        $this->beConstructedWith([
            'name' => 'testname',
            'phone' => 'testphone',
            'street' => 'teststreet',
        ]);
    }

    public function it_stores_proper_values_after_construct()
    {
        $this->offsetGet('name')->shouldBeEqualTo('testname');
        $this->offsetGet('phone')->shouldBeEqualTo('testphone');
        $this->offsetGet('street')->shouldBeEqualTo('teststreet');
    }

    public function its_values_can_be_changed_using_array_notation()
    {
        $this['name'] = 'updatedname';
        $this['phone'] = 'updatedphone';
        $this['street'] = 'updatedstreet';

        $this->offsetGet('name')->shouldBeEqualTo('updatedname');
        $this->offsetGet('phone')->shouldBeEqualTo('updatedphone');
        $this->offsetGet('street')->shouldBeEqualTo('updatedstreet');
    }

    public function its_values_can_be_changed_using_exchangeArray()
    {
        $this->exchangeArray([
            'name' => 'exchangename',
            'phone' => 'exchangephone',
            'street' => 'exchangestreet',
        ]);

        $this->offsetGet('name')->shouldBeEqualTo('exchangename');
        $this->offsetGet('phone')->shouldBeEqualTo('exchangephone');
        $this->offsetGet('street')->shouldBeEqualTo('exchangestreet');
    }
}
