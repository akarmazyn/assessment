<?php

namespace spec\TryCatch\Repository;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AddressCsvRepositorySpec extends ObjectBehavior
{
    /**
     * @var vfsStreamDirectory
     */
    private $workDir;

    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Repository\AddressCsvRepository');
    }

    public function let()
    {
        $this->workDir = vfsStream::setup('workdir');
        $file = vfsStream::newFile('example.csv');

        $csvContent = <<<CSVEND
Michal,506088156,Michalowskiego 41
Marcin,502145785,Opata Rybickiego 1
CSVEND;
        $file->setContent($csvContent);
        $this->workDir->addChild($file);

        $this->beConstructedWith(vfsStream::url('workdir/example.csv'));
    }

    public function it_reads_csv_files()
    {
        $this->getById(0)->shouldHaveKey('name');
        $this->getById(0)->offsetGet('name')->shouldBeEqualTo('Michal');
        $this->getById(0)->shouldHaveKey('phone');
        $this->getById(0)->offsetGet('phone')->shouldBeEqualTo('506088156');
        $this->getById(0)->shouldHaveKey('street');
        $this->getById(0)->offsetGet('street')->shouldBeEqualTo('Michalowskiego 41');

        $this->getById(1)->shouldHaveKey('name');
        $this->getById(1)->offsetGet('name')->shouldBeEqualTo('Marcin');
        $this->getById(1)->shouldHaveKey('phone');
        $this->getById(1)->offsetGet('phone')->shouldBeEqualTo('502145785');
        $this->getById(1)->shouldHaveKey('street');
        $this->getById(1)->offsetGet('street')->shouldBeEqualTo('Opata Rybickiego 1');
    }
}
