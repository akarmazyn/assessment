<?php

namespace spec\TryCatch\Validator;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TryCatch\Validator\Exception;

class AddressSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('TryCatch\Validator\Address');
    }

    public function it_throws_exception_on_invalid_data()
    {
        $this->shouldThrow(new Exception('Name field should not be empty'))
            ->duringIsValid(['name' => '']);

        $this->shouldThrow(new Exception('Phone field should not be empty'))
            ->duringIsValid(['name' => 'name', 'phone' => '']);

        $this->shouldThrow(new Exception('Street field should not be empty'))
            ->duringIsValid(['name' => 'name', 'phone' => 'phone', 'street' => '']);

        $this->shouldNotThrow('\TryCatch\Validator\Exception')
            ->duringIsValid(['name' => 'name', 'phone' => 'phone', 'street' => 'street']);
    }
}
