<?php
$container = new TryCatch\DI\Container();

$container['address.repository'] = new \TryCatch\Repository\AddressCsvRepository(__DIR__ . '/../data/example.csv');
$container['address.validator'] = new \TryCatch\Validator\Address();

return $container;
