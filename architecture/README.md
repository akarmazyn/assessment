Task Two - Architectural struggle
=================================

The attachment named example.php​is a simple php script. It's job is to serve one json­encoded string in response to http request. The script is also using `examples.csv` ​for fetching data. Your job is to refactor this script. Remove all mistakes and bad practices, create some structure of folders and files that will contain classes, design architecture as if this simple script was a small part of a much bigger project. In other words: pimp this script up!

This is more about architecture of your code and less about function itself. We will judge on structure, hierarchy of objects and overall design. Take into consideration how clean your code is and how easy it is to read and understand. Avoid tight coupling between any layer you introduce to this code and remember that this code should be easy for further extension by somebody else. You have total freedom in how to achieve this. There are, however, some rules as described below:

* You may not​use any external library or framework except from the ones used to Unit Test the solution
* After refactoring, this code must​do the same thing.
* You have to ​use MVC pattern (or similar)
* You could​use any other pattern if you feel like it
* You should​conform to programming principles
* You have to ​use OOP
* You have to ​do it in RESTful style
* You should​introduce some framework­like feature (routing, dispatching, autoloading)
* You have to ​use at least php 5.3 (namespace is mandatory)
* You can​use psr­0 autoloading system
* You can​put comments where you describe why you do certain things or just explaining some more high level decision
* You can​change storage system for contact data.
* All above rules are very important!

    * Have to, may not­ this is mandatory
    * could, should ­ this is strongly recommended
    * can ­ it's up to you

Setup:
------

If you already have `composer` installed globally, simply run (while being inside `architecture` folder):

```
composer install
```

Tests:
------

I've used [phpspec](http://www.phpspec.net) to test the solution, run the tests by executing:

```
vendor/bin/phpspec run
```

You can check the code coverage report:
```
cd coverage
php -S localhost:8000 
```
And go to [http://localhost:8000](http://localhost:8000) from your browser.

Run the application:
--------------------

Using PHP build-in server:

```
php -S localhost:8001 public/index.php
```

Now you can go to [http://localhost:8001/address](http://localhost:8001/address) to get the list of every address from the `example.csv` file.
To display a single address go to [http://localhost:8001/address/1](http://localhost:8001/address/1).

Basic CRUD:
-----------

You can test basic CRUD using some `REST client` directly from your browser or from CLI by running curl.

** CREATE **

```
curl -X POST http://localhost:8003/address --data '{"name":"name","phone":"phone","street":"street"}' --header "Content-Type:application/json"
```

** UPDATE **

```
curl -X PUT http://localhost:8003/address/3 --data '{"phone":"new phone number"}' --header "Content-Type:application/json"
```

** DELETE **

```
curl -X DELETE http://localhost:8003/address/3
```

