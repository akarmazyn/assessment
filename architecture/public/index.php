<?php
require_once __DIR__ . '/../vendor/autoload.php';

use TryCatch\Router\Route;

$application = new TryCatch\Application(require_once __DIR__ .'/../config/container.php');

$addressController = new TryCatch\Controller\Address();
$application['routes']->addRoute(
    new Route\Regex('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'show'], 'get')
);
$application['routes']->addRoute(
    new Route\Literal('/address', ['controller' => $addressController, 'action' => 'index'], 'get')
);
$application['routes']->addRoute(
    new Route\Literal('/address', ['controller' => $addressController, 'action' => 'create'], 'post')
);
$application['routes']->addRoute(
    new Route\Literal('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'update'], 'put')
);
$application['routes']->addRoute(
    new Route\Literal('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'delete'], 'delete')
);



//$application->get('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'show']);
//$application->get('/address', ['controller' => $addressController, 'action' => 'index']);
//$application->post('/address', ['controller' => $addressController, 'action' => 'create']);
//$application->put('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'update']);
//$application->delete('#/address/(\d+)#', ['controller' => $addressController, 'action' => 'delete']);

$application->run(new TryCatch\Http\Request());
