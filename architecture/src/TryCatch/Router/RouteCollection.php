<?php
namespace TryCatch\Router;

use TryCatch\Router\Route\RouteInterface;

/**
 * Class RouteCollection
 *
 * @package TryCatch\Router
 */
class RouteCollection
{
    /**
     * @var RouteInterface[]
     */
    private $routes;

    /**
     * @param RouteInterface $route
     */
    public function addRoute(RouteInterface $route)
    {
        $this->routes[] = $route;
    }

    /**
     * @return RouteInterface[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }
} 
