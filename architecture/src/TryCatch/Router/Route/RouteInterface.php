<?php
namespace TryCatch\Router\Route;

interface RouteInterface
{
    public function match($method, $path);
    public function getOptions();
}
