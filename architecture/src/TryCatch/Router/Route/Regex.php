<?php
namespace TryCatch\Router\Route;

/**
 * Class Regex
 *
 * @package TryCatch\Router\Route
 */
class Regex implements RouteInterface
{
    /**
     * @var string
     */
    private $pathRegex;

    /**
     * @var array
     */
    private $options;

    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $matches = [];

    /**
     * @param $pathRegex
     * @param $options
     * @param $method
     */
    public function __construct($pathRegex, $options, $method)
    {
        $this->pathRegex = $pathRegex;
        $this->options = $options;
        $this->method = $method;
    }

    /**
     * @param $method
     * @param $path
     * @return bool
     */
    public function match($method, $path)
    {
        return $method === $this->method && preg_match($this->pathRegex, $path, $this->matches);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        if (!empty($this->matches)) {
            unset($this->matches[0]);
        }
        return [$this->options, array_values($this->matches)];
    }
}
