<?php
namespace TryCatch\Router\Route;

/**
 * Class Literal
 *
 * @package TryCatch\Router\Route
 */
class Literal implements RouteInterface
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $options;

    /**
     * @var string
     */
    private $method;

    /**
     * @param $path
     * @param $options
     * @param $method
     */
    public function __construct($path, $options, $method)
    {
        $this->path = $path;
        $this->options = $options;
        $this->method = $method;
    }

    /**
     * @param $method
     * @param $path
     * @return bool
     */
    public function match($method, $path)
    {
        return $method === $this->method && $path === $this->path;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [$this->options, null];
    }
}
