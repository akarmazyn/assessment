<?php
namespace TryCatch\Repository;

abstract class AbstractCsvRepository implements CsvRepositoryInterface
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var bool
     */
    private $fileRead = false;

    /**
     * @param $filename
     */
    public function __construct($filename)
    {
        Exception::ensureValidFile($filename);
        $this->filename = $filename;
    }

    /**
     * @param string $mode
     *
     * @return resource
     */
    private function getFileToRead($mode = 'r')
    {
        return fopen($this->filename, $mode);
    }

    /**
     * @return array
     */
    protected function loadData()
    {
        if ($this->fileRead) {
            return $this->data;
        }

        $file = $this->getFileToRead();

        while (false !== ($line = fgetcsv($file))) {
            $this->data[] = $line;
        }

        fclose($file);

        $this->fileRead = true;

        return $this->data;
    }

    /**
     * Destroys data inside file, replaces it with provided data.
     *
     * @param $data
     */
    protected function rewriteData($data)
    {
        $file = $this->getFileToRead('w');

        foreach ($data as $model) {
            fputcsv($file, $model->getArrayCopy());
        }

        fclose($file);
    }

    protected function appendData(\ArrayObject $data)
    {
        $file = $this->getFileToRead('a+');

        fputcsv($file, $data->getArrayCopy());

        fclose($file);
    }

    /**
     * @return array
     */
    protected function getData()
    {
        if (!$this->fileRead) {
            $this->loadData();
        }

        return $this->data;
    }
} 
