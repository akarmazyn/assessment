<?php
namespace TryCatch\Repository;

use TryCatch\Model\Address;

class AddressCsvRepository extends AbstractCsvRepository
{
    /**
     * @param $id
     * @return User
     * @throws Exception
     */
    public function getById($id)
    {
        $data = $this->getData();

        if (isset($data[$id])) {
            return new Address($data[$id]);
        }

        throw new Exception(sprintf('Address with id "%s" does not exist', $id), 404);
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $data = $this->getData();
        $allUsers = [];
        foreach ($data as $userData) {
            $allUsers[] = new Address($userData);
        }

        return $allUsers;
    }

    /**
     * @param \ArrayObject $model
     */
    public function create(\ArrayObject $model)
    {
        $this->appendData($model);
    }

    /**
     * @param $id
     * @param \ArrayObject $model
     */
    public function update($id, \ArrayObject $model)
    {
        $data = $this->getAll();
        $data[$id] = $model;

        $this->rewriteData($data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $data = $this->getAll();
        unset($data[$id]);

        $this->rewriteData($data);
    }
}
