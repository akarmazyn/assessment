<?php
namespace TryCatch\Repository;

interface RepositoryInterface
{
    public function getById($id);
    public function getAll();
    public function create(\ArrayObject $model);
    public function update($id, \ArrayObject $model);
    public function delete($id);
}
