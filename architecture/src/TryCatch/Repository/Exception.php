<?php
namespace TryCatch\Repository;

/**
 * Class Exception.
 */
class Exception extends \Exception
{
    /**
     * @param $filename
     *
     * @throws Exception
     */
    public static function ensureValidFile($filename)
    {
        if (!file_exists($filename)) {
            throw new self(sprintf('File "%s" does not exist', $filename));
        }

        if (!is_writable($filename)) {
            throw new self(sprintf('File "%s" needs to be writable', $filename));
        }
    }
}
