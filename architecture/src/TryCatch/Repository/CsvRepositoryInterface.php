<?php
namespace TryCatch\Repository;

interface CsvRepositoryInterface extends RepositoryInterface
{
    public function __construct($filename);
}
