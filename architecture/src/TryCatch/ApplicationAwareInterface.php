<?php
namespace TryCatch;

interface ApplicationAwareInterface
{
    public function setApplication(Application $application);

    public function getApplication();
}
