<?php
namespace TryCatch\Controller;

use TryCatch\ApplicationAwareInterface;
use TryCatch\ApplicationAwareTrait;
use TryCatch\Http\JsonResponse;
use TryCatch\Http\RequestAwareInterface;
use TryCatch\Http\RequestAwareTrait;
use TryCatch\Validator\Exception;

/**
 * Class Address.
 */
class Address implements RequestAwareInterface, ApplicationAwareInterface
{
    use RequestAwareTrait;
    use ApplicationAwareTrait;

    /**
     *
     */
    public function index()
    {
        $response = new JsonResponse();
        $di = $this->getApplication()['di'];
        $response->setValues($di['address.repository']->getAll());

        return $response;
    }

    /**
     * @param array $params
     *
     * @return JsonResponse
     */
    public function show($params)
    {
        $id = $params[0];
        $di = $this->getApplication()['di'];

        $response = new JsonResponse();
        try {
            $response->setValues($di['address.repository']->getById($id));
        } catch (\TryCatch\Repository\Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setValues(['error' => $e->getMessage()]);
        }

        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function create()
    {
        $address = new \TryCatch\Model\Address($this->getRequest()->getBody());
        $di = $this->getApplication()['di'];

        $response = new JsonResponse();
        try {
            $validator = $di['address.validator'];

            if ($validator->isValid($address)) {
                $repository = $di['address.repository'];
                $repository->create($address);
            }

            $response->setStatusCode(201);
        } catch (Exception $e) {
            $response->setStatusCode(400);
            $response->setValues(['error' => $e->getMessage()]);
        }

        return $response;
    }

    /**
     * @param $params
     *
     * @return JsonResponse
     */
    public function update($params)
    {
        $id = $params[0];
        $di = $this->getApplication()['di'];
        $repository = $di['address.repository'];

        $response = new JsonResponse();
        try {
            $address = $di['address.repository']->getById($id);
        } catch (\TryCatch\Repository\Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setValues(['error' => $e->getMessage()]);
            return $response;
        }

        $data = $this->getRequest()->getBody();
        $address->exchangeArray(array_merge($address->getArrayCopy(), $data));

        try {
            $validator = $di['address.validator'];

            if ($validator->isValid($address)) {
                $repository->update($id, $address);
            }

            $response->setStatusCode(200);
        } catch (Exception $e) {
            $response->setStatusCode(400);
            $response->setValues(['error' => $e->getMessage()]);
        }

        return $response;
    }

    /**
     * @param $params
     *
     * @return JsonResponse
     */
    public function delete($params)
    {
        $id = $params[0];
        $di = $this->getApplication()['di'];
        $repository = $di['address.repository'];

        $response = new JsonResponse();
        try {
            $di['address.repository']->getById($id);
            $repository->delete($id);
        } catch (\TryCatch\Repository\Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setValues(['error' => $e->getMessage()]);
        }

        return $response;
    }
}
