<?php
namespace TryCatch\Model;

/**
 * Class Address
 *
 * @package TryCatch\Model
 */
class Address extends \ArrayObject
{
    private $fieldMap = [
        'name',
        'phone',
        'street'
    ];

    public function __construct($input = null, $flags = 0, $iterator_class = 'ArrayIterator')
    {
        if (!empty($input)) {
            $input = array_combine($this->fieldMap, $input);
        }

        parent::__construct($input, $flags, $iterator_class);
    }
}
