<?php
namespace TryCatch\Http;

/**
 * Class JsonResponse.
 */
class JsonResponse
{
    private $values = [];

    private $statusCode = 200;

    /**
     * @param array $values
     * @param int   $statusCode
     */
    public function __construct($values = [], $statusCode = 200)
    {
        $this->values = $values;

        $this->setStatusCode($statusCode);
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->values[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function get($name)
    {
        return $this->values[$name];
    }

    /**
     * @param $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        http_response_code($this->statusCode);

        return $this->getContent();
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return json_encode($this->values);
    }
}
