<?php
namespace TryCatch\Http;

/**
 * Class Request.
 */
class Request implements RequestInterface
{
    /**
     * @var array
     */
    private $get = [];

    /**
     * @var array
     */
    private $post = [];

    /**
     * @var array
     */
    private $server = [];

    /**
     * @var string
     */
    private $body;

    /**
     * @var bool
     */
    private $requestUri;

    public function __construct()
    {
        $this->setGetVariables($_GET);
        $this->setPostVariables($_POST);
        $this->setServerVariables($_SERVER);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestUri()
    {
        if ($this->requestUri) {
            return $this->requestUri;
        }

        if ($requestUri = $this->server('REQUEST_URI')) {
            $queryString = '?'.$this->server('QUERY_STRING');

            if (false !== strpos($requestUri, $queryString)) {
                $requestUri = substr($requestUri, 0, strpos($requestUri, $queryString));
            }
        }
        $this->requestUri = $requestUri;

        return $this->requestUri;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->server('REQUEST_METHOD');
    }

    /**
     * @param $name
     * @param null $default
     */
    public function get($name, $default = null)
    {
        return isset($this->get[$name]) ? $this->get[$name] : $default;
    }

    /**
     * @param $variables
     */
    private function setGetVariables($variables)
    {
        $this->get = $variables;
    }

    /**
     * @param $name
     * @param null $default
     */
    public function post($name, $default = null)
    {
        return isset($this->post[$name]) ? $this->post[$name] : $default;
    }

    /**
     * @param $variables
     */
    private function setPostVariables($variables)
    {
        $this->post = $variables;
    }

    /**
     * @param $name
     * @param null $default
     */
    public function server($name, $default = null)
    {
        return isset($this->server[$name]) ? $this->server[$name] : $default;
    }

    /**
     * @param $variables
     */
    private function setServerVariables($variables)
    {
        $this->server = $variables;
    }

    /**
     * @return mixed|string
     */
    public function getBody()
    {
        if (null === $this->body) {
            $this->body = file_get_contents('php://input');
        }

        if ('application/json' === $this->server('HTTP_CONTENT_TYPE')) {
            $this->body = json_decode($this->body, true);
        }

        return $this->body;
    }
}
