<?php
namespace TryCatch\Http;

interface RequestInterface
{
    /**
     * Returns HTTP method name.
     *
     * @return string
     */
    public function getMethod();

    /**
     * Returns request URI.
     *
     * @return string
     */
    public function getRequestUri();
}
