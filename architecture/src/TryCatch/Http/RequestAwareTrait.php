<?php
namespace TryCatch\Http;

/**
 * Class RequestAwareTrait.
 */
trait RequestAwareTrait
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param RequestInterface $request
     */
    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
    }
}
