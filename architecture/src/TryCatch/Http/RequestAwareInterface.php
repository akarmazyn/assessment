<?php
namespace TryCatch\Http;

interface RequestAwareInterface
{
    public function getRequest();

    public function setRequest(RequestInterface $request);
}
