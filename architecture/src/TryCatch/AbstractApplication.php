<?php
namespace TryCatch;

/**
 * Class AbstractApplication.
 */
abstract class AbstractApplication extends ArrayAccess
{
    /**
     * Dispatches the request, sets response.
     */
    protected function handleRequest()
    {
        /** @var Http\RequestInterface $request */
        $request = $this['request'];
        $routes = $this['routes'];
        $dispatcher = $this['dispatcher'];

        $this['response'] = $dispatcher->dispatch($request, $routes, $this);
    }
}
