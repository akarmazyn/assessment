<?php
namespace TryCatch\MVC;

use TryCatch\Application;
use TryCatch\ApplicationAwareInterface;
use TryCatch\Http\RequestAwareInterface;
use TryCatch\Http\RequestInterface;
use TryCatch\Router\RouteCollection;
use TryCatch\Router\RouterException;

/**
 * Class Dispatcher.
 */
class Dispatcher
{
    /**
     * @param RequestInterface $request
     * @param RouteCollection $routes
     * @param Application $application
     *
     * @return mixed
     *
     * @throws \TryCatch\Router\RouterException
     */
    public function dispatch(RequestInterface $request, RouteCollection $routes, Application $application)
    {
        $method = strtolower($request->getMethod());

        foreach ($routes->getRoutes() as $route) {
            if ($route->match($method, $request->getRequestUri())) {
                list($callback, $params) = $route->getOptions();

                if (is_callable($callback)) {
                    return call_user_func($callback, $params);
                }

                $controller = $callback['controller'];

                if ($controller instanceof RequestAwareInterface) {
                    $controller->setRequest($request);
                }
                if ($controller instanceof ApplicationAwareInterface) {
                    $controller->setApplication($application);
                }

                return call_user_func([$controller, $callback['action']], $params);
            }
        }

        throw new RouterException(sprintf('There is no "%s" route registered for method "%s"', $request->getRequestUri(), $method));
    }
}
