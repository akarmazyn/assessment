<?php
namespace TryCatch\Validator;

interface ValidatorInterface
{
    public function isValid($data);
}
