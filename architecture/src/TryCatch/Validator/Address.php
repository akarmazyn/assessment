<?php
namespace TryCatch\Validator;

/**
 * Class Address.
 */
class Address implements ValidatorInterface
{
    /**
     * @param $data
     *
     * @return bool
     *
     * @throws Exception
     */
    public function isValid($data)
    {
        if (!isset($data['name']) || empty($data['name'])) {
            throw new Exception('Name field should not be empty');
        }

        if (!isset($data['phone']) || empty($data['phone'])) {
            throw new Exception('Phone field should not be empty');
        }

        if (!isset($data['street']) || empty($data['street'])) {
            throw new Exception('Street field should not be empty');
        }

        return true;
    }
}
