<?php
namespace TryCatch\DI;

use TryCatch\ArrayAccess;

/**
 * Class Container.
 */
class Container extends ArrayAccess
{
    /**
     * @param array $values
     */
    public function __construct($values = [])
    {
        foreach ($values as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }
}
