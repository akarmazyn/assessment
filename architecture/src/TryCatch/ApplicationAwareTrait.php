<?php
namespace TryCatch;

trait ApplicationAwareTrait
{
    /**
     * @var Application
     */
    private $application;

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param Application $application
     */
    public function setApplication(Application $application)
    {
        $this->application = $application;
    }
}
