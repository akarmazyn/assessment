<?php
namespace TryCatch;

use TryCatch\DI\Container;
use TryCatch\Http\Request;
use TryCatch\Http\RequestInterface;
use TryCatch\MVC\Dispatcher;
use TryCatch\Router\RouteCollection;

/**
 * Class Application.
 */
class Application extends AbstractApplication
{
    public function __construct(Container $container = null)
    {
        $this['dispatcher'] = new Dispatcher();
        $this['routes'] = new RouteCollection();

        if (!$container) {
            $container = new Container();
        }
        $this['di'] = $container;
    }

    /**
     * @param RequestInterface $request
     */
    public function run(RequestInterface $request = null)
    {
        if (!$request) {
            $request = new Request();
        }
        $this['request'] = $request;

        $this->handleRequest();
        $this->sendResponse();
    }

    /**
     *
     */
    private function sendResponse()
    {
        echo $this['response'];
    }
}
